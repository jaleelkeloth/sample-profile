import { BrowserRouter as Router, Routes, Route, Navigate, Outlet } from 'react-router-dom';
import Auth from "./Modules/Auth/Auth";
import Profile from "./Modules/Profile/Profile";
import PageNotFound from "./Components/PageNotFound/PageNotFound";

//Check if the user is logged in and load the component(rendered by outlet), else redirect to login page
const SecretRoute = () => {
  return window.localStorage.user === undefined ? <Navigate to="/login" replace={true} /> : <Outlet />
};

//Check if the user is logged in and redirect to dashboard page, else continue with login screen itself(rendered by outlet)
const LoginRoute = () => {
  return window.localStorage.user !== undefined ? <Navigate to="/profile" replace={true} /> : <Outlet />
};

const LandingPage = () => {
  return window.localStorage.user !== undefined ? <Navigate to="/profile" replace={true} /> : <Navigate to="/login" replace={true} />
}

const Path = (
  <Router>
    <Routes>
      <Route exact path="/" element={<LandingPage />} />
      <Route exact path="/login" element={<LoginRoute />} >
        <Route exact path="/login" element={<Auth />} />
      </Route>
      <Route exact path="/profile" element={<SecretRoute />} >
        <Route exact path="/profile" element={<Profile />} />
      </Route>
      <Route path="*" element={<PageNotFound />} />
    </Routes>
  </Router>
);

export default Path;
