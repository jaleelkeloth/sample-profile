import React from "react";
import styles from './PageNotFound.module.css';

function PageNotFound() {
  return (
    <div className={styles.container}>    
      <img src="404.png" alt="404 page. Cat ate your files in this location." className={styles.img}/>
    </div>
  );
};

export default PageNotFound;
