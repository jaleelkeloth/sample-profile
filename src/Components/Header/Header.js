import React from "react";
import styles from './Header.module.css';

function Header(props) {

  return (
    <div className={styles.header}>
      <nav className={styles.NavbarItems}>
        <div className={styles.logo}>
          <h3>
            <img src={'logo.jpg'} alt="Payu Logo" className={styles.img} />
            </h3>
          </div>
        </nav>
      </div>
    );
};

export default Header;
