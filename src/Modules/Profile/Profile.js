import React from "react";
import styles from './Profile.module.css';
import { useNavigate } from "react-router";

function Profile() {

    const nav = useNavigate();

    const handleLogout = () => {
      window.localStorage.removeItem('user');
      nav('/login');
    }

    return (
      <div className={styles.container}>
        <button onClick={handleLogout} class="btn1 btn-primary float-end">Logout</button>
        <div class="container mt-4 mb-4 p-3 d-flex justify-content-center">
          <div class="card p-4">
            <div class=" image d-flex flex-column justify-content-center align-items-center">
              <button class="btn btn-secondary">
                <img src="myAvatar.svg" height="100" width="100" alt="Avatar depiction"/>
              </button>
            <span class="name mt-3">Abdul Jaleel Keloth</span>
            <span class="idd">@jaleelkeloth</span>
            <div class="d-flex mt-2">
              <button class="btn1 btn-dark">Edit Profile</button>
            </div>
            <div class="text mt-3">
              <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default Profile;
