import React from "react";
import { GoogleLogin } from '@react-oauth/google';
import {useNavigate} from 'react-router-dom';
import styles from './Auth.module.css';

function Auth() {
  let nav = useNavigate();
  return (
    <div className={styles.auth}>
      <GoogleLogin
        onSuccess={credentialResponse => {
            console.log(credentialResponse);
            localStorage.setItem('user', credentialResponse.credential);
            nav('/profile');
        }}
        onError={() => {
            console.log('Login Failed');
        }}
      />
    </div>
  );
};

export default Auth;
