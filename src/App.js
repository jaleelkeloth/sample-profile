import styles from './App.module.css';
import Path from './Path';
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
  
function App() {
  return (
    <div className={styles.app}>
      <Header />
      {Path}
      <Footer />
    </div>
  );
}

export default App;
