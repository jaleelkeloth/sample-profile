# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

## `Libraries used`

### `react-router-dom`

This library is used for handling the routing to different pages in the application.\
Implementation can be seen in the file Path.js in the root folder.

### `@react-oauth/google`

This library is used for handling google auth with latest google identity sign in.

### `Bootstrap`

This library is used for bringing in responsiveness and to use default styles from the library.

## `Setting it up locally`

Once the repository is cloned, run command `npm install`.\
Then run the command `npm start`. Application will available at http://localhost:3000.

### `Test specific workarounds`

The token recieved from google auth is saved in the localstorage, which is not the ideal way.\
Ideally would have followed the `auth-code` flow with google sign in and pass the code to backend.\
Backend service would send `access_token` and `refresh_token` in the response and save it in browser cookie.\
Since client side cannot access the cookie values saved by backend response, we can avoid code stealing token.\

Another is, ideally .env would not be saved to git repository to avoid client-id and sensitive information leaks.\

## `Folder Structure`

All the code is inside the src folder. There are two sub folders, Components and Modules.\
Pure coponents, that do not have any sideeffects are add to Components folder.\
Unpure components are added to the Modules folder. Routes are handled with Path.js file.\

## `Deployment`

Deployed in gcp compute engine using nginx and is available here http://35.222.149.38/login
But google login won't work due to issue Invalid Origin: must end with a public top-level domain (such as .com or .org)\
while adding the redirect uri.

